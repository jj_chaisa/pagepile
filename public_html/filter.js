var filters = [
] ;

var templates = {
	import : { locked:true, label:'Start with' , desc:'Start with an existing pile; always the first step!' , params:[{name:'pile',type:'number',label:'Pile ID'}] } ,
	union : {
		label : 'Union' ,
		desc : 'Merges two piles, removing duplicates. Both piles must be in the same wiki.' ,
		params : [ {name:'pile',type:'number',label:'Pile ID to merge'} ]
	} ,
	subset : {
		label : 'Subset' ,
		desc : 'Keeps only pages present in both piles. Both piles must be in the same wiki.' ,
		params : [ {name:'pile',type:'number',label:'Pile ID to subset'} ]
	} ,
	exclusive : {
		label : 'Exclusive' ,
		desc : 'Remove pages from this pile which are present in another pile. Both piles must be in the same wiki.' ,
		params : [ {name:'pile',type:'number',label:'Pile ID to subset'} ]
	} ,
	follow_redirects : {
		label : 'Follow redirects' ,
		desc : 'Follows (resolves) redirects.' ,
		params : []
	} ,
	filter_namespace : {
		label : 'Filter namespace' ,
		desc : 'EXPERIMENTAL DO NOT USE! Keeps or removes pages based on their namespace.' ,
		params : [
			{name:'keep',type:'text',label:'keep all pages with namespace(s)',placeholder:'0,1,...',optional:true } ,
			{name:'remove',type:'text',label:'remove all pages with namespace(s)',placeholder:'0,1,...',optional:true }
		]
	} ,
	to_wikidata : {
		label : 'To Wikidata' ,
		desc : 'Converts pages in a pile to their respective Wikidata items. Pages without a corresponding Wikidata item are removed.' ,
		params : []
	} ,
	from_wikidata : {
		label : 'From Wikidata' ,
		desc : 'Converts Wikidata items into pages on a wiki. Items without a corresponding page on that wiki are removed.' ,
		params : [ {name:'wiki',type:'text',label:'Target Wiki',placeholder:'e.g. enwiki or dewikisource'} ]
	} ,
	no_wikidata : {
		label : 'No Wikidata' ,
		desc : 'Keeps only pages that have no Wikidata item.' ,
		params : []
	} ,
	random_subset : {
		label : 'Random subset' ,
		desc : 'Reduces the pile to a random subset.' ,
		params : [ {name:'size',type:'number',label:'number of pages to keep'} ]
	} ,
} ;

function escattr ( s ) {
	return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#x27;').replace(/\//g,'&#x2F;') ;
}


function renderAppendFilter ( filter ) {
	var h = "<div class='filter_wrapper' filter_id='" + filter.num + "'>" ;
	
	
	h += "<div>" ;
	h += "<div class='filter_part'>#" + (filter.num+1) + "</div>" ;
	h += "<div class='filter_part'><b>" + filter.label + "</b></div>" ;
	
	$.each ( (filter.params||[]) , function ( k , v ) {
		h += "<div class='filter_part'>" ;
		h += v.name + ": " ;
		h += "<input type='" + v.type + "' param_num='" + k + "' filter_num='" + filter.num + "' class='filter_param'" ;
		if ( typeof v.placeholder != 'undefined' ) h += " placeholder='" + v.placeholder + "'" ;
		if ( typeof v.value != 'undefined' ) h += " value='" + v.value + "'" ;
		if ( v.optional ) h += " optional=1" ;
		h += " /></div>" ;
	} ) ;

	h += "<div class='pull-right' style='font-size:9pt'>" ;
	
	h += '<div class="btn-group">' ;
	h += '<a class="btn btn-info dropdown-toggle btn-small" data-toggle="dropdown" href="#">Add after <span class="caret"></span></a>' ;
	h += '<ul class="dropdown-menu">' ;
	$.each ( templates , function ( k , v ) {
		if ( v.locked ) return ;
		h += "<li>" ;
		h += "<a href='#' class='add_after_filter' filter_name='" + k + "' filter_num='" + filter.num + "' data-toggle='tooltip' title='" + escattr(v.desc||'') + "'>" + v.label + "</a>" ;
		h += "</li>" ;
	} ) ;
	h += "</ul></div>" ;
	
	if ( !filter.locked ) {
		h += "<div>" ;
		h += "<button class='btn btn-danger btn-small delete_filter' filter_num='" + filter.num + "' title='Delete this filter step'>Delete</button>" ;
		if ( filter.num > 0 && !filters[filter.num-1].locked ) {
			h += " <button class='btn  btn-small move_filter_up' filter_num='" + filter.num + "' title='Move filter up one position'>&uArr;</button>" ;
		}
		if ( filter.num+1 < filters.length ) {
			h += " <button class='btn  btn-small move_filter_down' filter_num='" + filter.num + "' title='Move filter down one position'>&dArr;</button>" ;
		}
		h += "</div>" ;
	}
	h += "</div>" ;
	
	h += "</div>" ;

	if ( typeof filter.desc != 'undefined' ) h += "<div class='filter_desc'>" + filter.desc + "</div>" ;
	h += "</div>" ;
	$('#filters').append ( h ) ;
}

var undo = [] ;

function cacheUndo(action) {
	var o = $.extend ( {} , { filters:$.extend([],filters) , action:action } ) ;
	undo.push ( o ) ;
	$('#undo').show() ;
}

function runFilters () {
	$('#run_output').html('<i>Running filters, please stand by...</i>') ;
	$.get ( './api.php' , {
		action : 'run_filters' ,
		filters : JSON.stringify ( filters )
	} , function ( d ) {

		var h = "<h3>Results</h3><table class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th>#</th><th>Action</th><th>PagePile</th><th>Pages</th></tr></thead>" ;
		h += "<tbody>" + d.log.join("") + "</tbody></table>" ;
		$('#run_output').html(h) ;
	} , 'json' ) ;
}

function checkFilterParams () {
	var bad = false ;
	$('input.filter_param').removeClass ( 'badParameter' ) ;
	$('input.filter_param').each ( function () {
		var o = $(this) ;
		var type = o.attr('type') ;
		if ( type == 'text' || type == 'number' ) {
			if ( o.val() == '' && o.attr('optional') != '1' ) {
				o.addClass ( 'badParameter' ) ;
				bad = true ;
			}
		}
	} ) ;
	
	if ( bad ) {
		$('#run_output').html('<i style="color:#FF4848">Please fill in the missing parameters!</i>') ;
	} else {
		runFilters() ;
	}
}

function renderFilters () {
	$('#filters').html('') ;
	$.each ( filters , function ( num , filter ) {
		filter.num = num ;
		renderAppendFilter ( filter ) ;
	} ) ;
	
	var h = "<div style='text-align:right;margin-top:20px;padding-top:10px;border-top:1px solid #EEE'>" ;
	h += "<button class='btn btn-info' style='display:none' id='undo'>Undo</button>" ;
	h += " <button class='btn btn-primary' id='run'>Run filters</button>" ;
	h += "</div>" ;
	
	h += "<div id='run_output'></div>" ;
	
	h += "<div style='margin-top:20px;padding-top:10px;border-top:1px dotted #DDD'>" ;
	h += "<p>You can store the above filter set in PasteBin. You will get a new page with a URL; keep the last part of that URL (\"/view/XXXXX\"). You can later load the filter set here by entering the PasteBin ID.</p>" ;
	h += "<div style='display:inline-block'>" ;
	h += "<form id='store_pastebin' class='form form-inline inline-form' method='post' target='_blank' action='https://tools.wmflabs.org/paste/api/create'><button class='btn btn-success' id='store'>Store filter set in WMF PasteBin</button>" ;
	h += "<input type='hidden' name='title' value='Filter set from PagePile' />" ;
	h += "<input type='hidden' name='text' value='' /></form>" ;
	h += "</div>" ;
	h += " <i>or</i> <div style='display:inline-block'>" ;
	h += "<form id='load_pastebin' class='form form-inline inline-form'><input type='text' id='pastebin_id' value='' placeholder='ID from WMF PasteBin'/><input type='submit' class='btn btn-success' id='load' value='Load from WMF PasteBin' /></form>" ;
	h += "</div>" ;
	h += " <a href='/paste/' target='_blank'>Check out PasteBin</a>" ;
	h += "</div>" ;

	$('#filters').append ( h ) ;
	
	
	$('#run').click ( function () {
		checkFilterParams() ;
		return false ;
	} ) ;
	
	$('#filters input.filter_param').keyup ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		var param_num = o.attr('param_num')*1 ;
		filters[filter_num].params[param_num].value = o.val() ;
		o.removeClass ( 'badParameter' ) ;
	} ) ;
	
	$('#filters a.add_after_filter').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		var filter_name = o.attr('filter_name') ;
		cacheUndo('add filter') ;
		insertNewFilter ( filter_name , filter_num+1 ) ;
		renderFilters() ;
		return false ;
	} ) ;
	
	$('#filters button.delete_filter').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		cacheUndo('delete filter') ;
		filters.splice ( filter_num , 1 ) ;
		renderFilters() ;
	} ) ;
	
	$('#filters button.move_filter_up').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		cacheUndo('move filter up') ;
		var tmp = filters[filter_num-1] ;
		filters[filter_num-1] = filters[filter_num] ;
		filters[filter_num] = tmp ;
		renderFilters() ;
	} ) ;
	
	$('#filters button.move_filter_down').click ( function () {
		var o = $(this) ;
		var filter_num = o.attr('filter_num')*1 ;
		cacheUndo('move filter down') ;
		var tmp = filters[filter_num+1] ;
		filters[filter_num+1] = filters[filter_num] ;
		filters[filter_num] = tmp ;
		renderFilters() ;
	} ) ;
	
	$('#load_pastebin').submit ( function () {
		var id = $('#pastebin_id').val() ;
		var url = "https://tools.wmflabs.org/paste/view/raw/" + id ;
		$.get ( url , function ( d ) {
			d = JSON.parse ( d ) ;
			cacheUndo('load filter set') ;
			filters = d ;
			renderFilters() ;
		} , 'text' ) ;
		return false ;
	} ) ;
	
	$('#undo').click ( function () {
		var o = undo.pop() ;
		filters = o.filters ;
		renderFilters() ;
	} ) ;
	
	if ( undo.length > 0 ) {
		$('#undo').text("Undo "+undo[undo.length-1].action).show() ;
	}

	$('#store_pastebin input[name="text"]').val ( JSON.stringify ( filters ) ) ;
	
}

function insertNewFilter ( filter_name , position ) {
	var filter = $.extend ( {} , templates[filter_name] ) ;
	filter.type = filter_name ;
	filters.splice ( position, 0 , filter ) ;
}

$(document).ready ( function () {
	insertNewFilter ( 'import' , 0 ) ;
	if ( start_pile != 0 ) filters[0].params[0].value = start_pile ;
	renderFilters() ;
} ) ;